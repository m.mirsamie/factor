/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'fa';
        config.toolbar = 'Basic';
        config.font_names = 'Droidnaskh;BYekan;BNazanin;BNazanin;Mitra;BTitr;Tahoma;Times New Roman;Georgia;'+
				'Arial; Verdana; Tahoma;Courier New;';
        // config.uiColor = '#AADC6E';
};
