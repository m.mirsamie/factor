var factor_jam = 0;
$(document).ready(function(){
    intial();
});
function intial(){
    CKEDITOR.replace('tmp_area');
    editor = CKEDITOR.instances['tmp_area'];
    editor.on('change',function(){
       var tmp_txt = String(editor.getData());
       selected_obj.html(tmp_txt);
    });
    $(document).click(function(){
        calTakhfif();
    });
    $("tr.maintable_row td,.editable").click(function(e){
        if($(this).hasClass('gc-8'))
        {
            if($(this).find('img').length===1)
                window.open($(this).find('img').prop('src'));
        }
        else if(!$(this).hasClass('gc-6'))
            td_click(this);
    });
    $("#tmp_area1").keyup(function(e){
        var tmp_txt = String($("#tmp_area1").val());
        tmp_txt = tmp_txt.replace(/\n/g,'<br>');
        if(selected_obj.hasClass('gc-5'))
        {
            if($.trim(tmp_txt)!=='')
                tmp_txt = monize2(tmp_txt);
            else
                tmp_txt = '';
        }
        if(!selected_obj.hasClass('gc-6'))
            selected_obj.html(tmp_txt);
        if(selected_obj.prop('id')==='sum_1' || selected_obj.prop('id')==='sum_2' || selected_obj.prop('id')==='sum_3')
        {
            var ad = parseInt($.trim(tmp_txt));
            if(!isNaN(ad))
            {
                selected_obj.html(monize2(tmp_txt));
                $("#jam_kol").html('جمع کل :  '+convert(ad)+' ریال ');
            }
        }
    });
    $(".config").click(function(){
        if($(this).hasClass('tick'))
            factor[String($(this).prop('id')).replace(/tick_/g,"")] = $(this).prop('checked')?'0':'';
        else        
            factor[$(this).prop('id')] = $(this).prop('checked')?'1':'0';
        loadConfigs();
    });
    factor_data = ($.trim(factor.body_json)!=='')?JSON.parse(factor.body_json):{};
    loadConfigs();
    if(typeof factor_data==='object')
        set_factor_data();
    calTakhfif();
}
function td_click(dobj){
    selected_obj = $(dobj);
    selected_text = $(dobj).html();
    if(!$(dobj).hasClass('gc-2') && !$(dobj).hasClass('gc-3') && $(dobj).prop('id')!=='header' && $(dobj).prop('id')!=='footer'){
        if($(dobj).prop('id')==='sum_1' || $(dobj).prop('id')==='sum_2' || $(dobj).prop('id')==='sum_3')
            selected_text = umonize(selected_text);
        $("#tmp_area1").val(selected_text);
        $("#tmp_area1").focus();
    }else
    {
        editor.setData(selected_text);
        showModal();
    }
}
function showModal(){
    $('#myModal').modal('show');
}
function closeModal(){
    $('#myModal').modal('hide');
}
function footerChange(dobj){
    //selected_text = selected_text.replace(/\|/g,"\"");
    $("#footer").html($(dobj).val().replace(/\|/g,"\""));
}
function matnChange(dobj){
    if(selected_obj)
    {
        var dat = $(dobj).find('option:selected').data();
        if(typeof dat['model']!=='undefined')
        {
            var img_obj = selected_obj.parent().find('.gc-8');
            var model_obj = selected_obj.parent().find('.gc-3');
            var ghimat_obj = selected_obj.parent().find('.gc-5');
            selected_obj  = selected_obj.parent().find('.gc-2');
            selected_text = $(dobj).val();
            //selected_text = selected_text.replace(/\n/g,"<br/>");
            selected_text = selected_text.replace(/\|/g,"\"");
            selected_obj.html(selected_text);
            //$("#tmp_area").val(selected_text);
            //$("#tmp_area").focus();
            img_obj.html('<img class="gc-img-factor" src="'+dat.img+'" alt="تصویر" />');
            model_obj.html(dat.model);
            ghimat_obj.html(monize2(dat.ghimat));
            //editor.setData(selected_text);
            //showModal();
        }
    }
}
function set_factor_data()
{
    var i,j;
    for(i in factor_data){
        if($(".row_"+String(i)).length===0)
            addRow();
        for(j in factor_data[i])
            $(".row_"+String(i)+" td."+j).html(factor_data[i][j]);
    }
    for(j in factor)
    {
        if($("#"+j).length===1)
        {
            if($("#"+j).hasClass('config'))
            {
                $("#"+j).prop('checked',factor[j]==='1');
            }
            else if($("#"+j).hasClass('hasValue'))
            {
                $("#"+j).val(factor[j]);
            }
            else
            {
                $("#"+j).html(factor[j]);
            }
        }
        if($("#tick_"+j).length===1)
        {
            if($.trim(factor[j])!== '')
                $("#tick_"+j).prop('checked',true);
            else
                $("#tick_"+j).prop('checked',false);
        }
    }
}
function gather_factor_data()
{
    factor_data = {};
    $("tr.maintable_row td").each(function(id,field){
        var td_class = $(field).prop('class');
        if($.trim(td_class).split('-')[0]==='gc')
        {
            var row_number = getTDRow($(field));
            if(typeof factor_data[row_number] === 'undefined')
                factor_data[row_number] = {};
            factor_data[row_number][$(field).prop('class')]=$(field).html();
        }
    });
}
function getTDRow(obj)
{
    return(parseInt(obj.parent().prop("class").split(' ')[obj.parent().prop("class").split(' ').length-1].split('_')[1],10));
}
function toggleCol(col_number,state)
{
    $(".gc-"+col_number).each(function(id,field){
        if(typeof state === 'undefined')
        {
            if(!$(field).is(":visible"))
                $(field).show();
            else
                $(field).hide();
        }
        else
        {
            if(state === true)
                $(field).show();
            else
                $(field).hide();  
        }
    });
}
function addRow()
{
    //var class_odd = ($(".page_a4 table tr.maintable_row:last").hasClass('maintable_odd'))?'maintable_even':'maintable_odd';
    var last_row = getTDRow($(".page_a4 table tr.maintable_row:last td"))+1;
    var last_index = parseInt($(".page_a4 table tr.maintable_row:last .gc-1").html().trim(),10);
    if(isNaN(last_index))
        last_index = 0;
    last_index++;
    var td = '';
    var disp = '';
    var j = 1;
    for(var i = 1;i <= 8;i++)
    {
        disp = '';
        if(i===6 && parseInt(factor.row_7,10) === 0)
            disp = 'style="display:none;"';
        if(i===8 && parseInt(factor.row_8,10) === 0)
            disp = 'style="display:none;"';
        j=i;
        if(i===6)
            j=7;
        if(i===7)
            j=6;
        td += '<td onclick="td_click(this);" class="gc-'+j+'" '+disp+'>'+((i===1)?last_index:'')+'</td>';
    }
    var tr='<tr class="new_row maintable_row row_'+last_row+'">'+td+'</tr>';    
    $(".page_a4 table tr.maintable_row:last").after(tr);
}
function removeRow()
{
    if(confirm('آیا سطر حذف شود؟'))
    {
        var obj = selected_obj.parent();
        if(obj)
        {
            obj.slideUp(function(){
                obj.remove();
                $("#tmp_area").val('');
            });
        }
    }
}
function sendFactor(no_submit)
{
    gather_factor_data();
    factor['body_json'] = JSON.stringify(factor_data);
    var i;
    for(i in factor)
        if($("#"+i).length>0)
        {
                if($("#"+i).hasClass('hasValue'))
                    factor[i] = $("#"+i).val();
                else if($("#"+i).hasClass('config'))
                    factor[i] = ($("#"+i).prop('checked'))?'1':'0';
                else
                    factor[i] = $("#"+i).html();
        }
    $("#gc-data").val(JSON.stringify(factor));
    if(typeof no_submit==='undefined')
        $("#form_data").submit();
}
function loadConfigs()
{
    var show_bg = parseInt(factor.has_bg,10);
    showBackground((!isNaN(show_bg) && show_bg>0)?1:0);
    if(factor.row_7 === '0' || $.trim(factor.row_7) === '')
        toggleCol(7,false);
    else
        toggleCol(7,true);
    if(factor.row_8 === '0' || $.trim(factor.row_8) === '')
        toggleCol(8,false);
    else
        toggleCol(8,true);
    var colspan = 4+(($(".gc-7").is(":visible"))?1:0);//+(($(".gc-8").is(":visible"))?1:0);
    $(".td_sum").each(function(id,field){
        $(field).prop("colspan",colspan);
    });
    $("#sum_1").prop("colspan",(($(".gc-8").is(":visible"))?2:1));
    $("#sum_2").prop("colspan",(($(".gc-8").is(":visible"))?2:1));
    $("#sum_3").prop("colspan",(($(".gc-8").is(":visible"))?2:1));
    $("#sum_4").prop("colspan",(($(".gc-8").is(":visible"))?2:1));
    if ($.trim(factor.sum_1)!=='')
        $("#sum_1").parent().show();
    else
        $("#sum_1").parent().hide();
    if ($.trim(factor.sum_2)!=='')
        $("#sum_2").parent().show();
    else
        $("#sum_2").parent().hide();
    if ($.trim(factor.sum_3)!=='')
        $("#sum_3").parent().show();
    else
        $("#sum_3").parent().hide();
    if ($.trim(factor.sum_4)!=='')
        $("#sum_4").parent().show();
    else
        $("#sum_4").parent().hide();
    change_header_margin();
    change_footer_margin();
    changeFooterHeader();
}
function calTakhfif()
{
    factor_jam = 0;
    $(".maintable_row").each(function(id,field){
        var tedad = parseInt($(field).find('.gc-4').html().trim(),10);
        var ghimat = parseInt(umonize($(field).find('.gc-5').html().trim()),10);
        var takhfif = parseFloat($(field).find('.gc-7').html().trim());
        tedad = (isNaN(tedad))?0:tedad;
        ghimat = (isNaN(ghimat))?0:ghimat;
        takhfif = (isNaN(takhfif))?0:takhfif;
        var takhfif_darsad = (factor.takhfif_darsad==='1');
        var ghimat_kol = tedad*ghimat;
        ghimat_kol -= ((takhfif_darsad)?ghimat_kol*takhfif/100:takhfif);
        $(field).find('.gc-6').html(monize2(parseInt(ghimat_kol,10)));
        factor_jam += ghimat_kol;
    });
    var takhf = parseInt(umonize($("#sum_2").html().trim()),10);
    $("#takhfif_kol").html('تخفیف : '+convert(takhf)+' ریال ');
    var factor_jam_kol = parseInt(umonize($("#sum_1").html().trim()),10);
    $("#jam_kol").html('جمع کل : '+convert(factor_jam_kol)+' ریال ');
    //console.log('takhf',takhf);
    //console.log('factor_jam_kol',factor_jam_kol);
    var mali = (!isNaN(factor_jam_kol)?factor_jam_kol:0) - (!isNaN(takhf)?takhf:0);
    if(mali < 0)
        mali = 0;
    var maliat = parseInt(mali*0.09,10);
    $("#tax").html(' مالیات برارزش افزوده : '+convert(maliat)+' ریال ');
    //console.log('mali',mali);
    $("#sum_3").html(monize2(maliat));
    //$("#sum_1").html(monize2(factor_jam)+'<br/>'+convert(factor_jam));
    var ghabel = mali + (!isNaN(maliat)?maliat:0);
    $("#ghabel_pardakht").html(' قابل پرداخت : '+convert(ghabel)+' ریال ');
    //console.log(ghabel);
    $("#sum_4").html(monize2(ghabel));
    
}
function monize2(inp){
    var out;
    var sht=String(inp).replace(/,/gi,'');
    var txt = sht.split('');
    var j=-1;
    var tmp='';
    for(var i=txt.length-1;i>=0;i--){
        if(j<2){
            j++;
            tmp=txt[i]+tmp;
        }else{
            j=0;
            tmp=txt[i]+','+tmp;
        }
    out=tmp;
    }
    return(out);
}
function umonize(inp){
    var out='0';
    var sht=inp.replace(/,/gi,'');
    sht=sht.replace(/\./gi,'');
    out=sht;
    return(out);
}
function monize(obj){
    var sht=String(obj.value).replace(/,/gi,'');
    var txt = sht.split('');
    var j=-1;
    var tmp='';
    for(var i=txt.length-1;i>=0;i--){
            if(j<2){
                    j++;
                    tmp=txt[i]+tmp;
            }else{
                    j=0;
                    tmp=txt[i]+','+tmp;
            }
    }
    obj.value = tmp;
}


function underThous(inpp)
{
	var out = '';
	var convert_small = {1:'یک',2:'دو',3:'سه',4:'چهار',5:'پنج',6:'شش',7:'هفت',8:'هشت',9:'نه'};
	var convert_middle = {10:'ده',11:'یازده',12:'دوازده',13:'سیزده',14:'چهارده',15:'پانزده',16:'شانزده',17:'هفده',18:'هجده',19:'نوزده',20:'بیست',30:'سی',40:'چهل',50:'پنجاه',60:'شصت',70:'هفتاد',80:'هشتاد',90:'نود',100:'صد'};
	var convert_large = {100:'صد',200:'دویست',300:'سیصد',400:'چهارصد',500:'پانصد',600:'ششصد',700:'هفتصد',800:'هشتصد',900:'نهصد',1000:'هزار'};
	if(inpp < 1000)
	{
		var out_arr = [];
		var kharej = inpp - (inpp % 100);
		if(typeof convert_large[kharej] !== 'undefined')
			out_arr.push(convert_large[kharej]);
		inpp -= kharej;
		if(inpp > 20)
			var kharej = inpp - (inpp % 10);
		else
			var kharej = inpp;
		if(typeof convert_middle[kharej] !== 'undefined')
			out_arr.push(convert_middle[kharej]);
		if(inpp > 20 || inpp < 10)
		{
			if(inpp > 20)
				inpp -= kharej;
			if(typeof convert_small[inpp] !== 'undefined')
				out_arr.push(convert_small[inpp]);
		}
                var out_tmp = [];
                for(var i = 0;i < out_arr.length;i++)
                    if($.trim(out_arr[i])!=='')
                        out_tmp.push(out_arr[i]);
                //console.log('under',out_arr);
		out = out_tmp.join(' و ');
	}
	return(out);
}
function convert_1(inp)
{
	var convert_level = ['','هزار','میلیون','میلیارد','تریلیارد'];
	var out = '';
	var inp = parseInt(inp,10);
	if(typeof inp !== 'undefined' && !isNaN(inp))
	{
		var maxLevel = 0;
		var maxLevelFound = false;
		for(var i = 0 ; i < 5 && !maxLevelFound; i++)
		{
			var kharej = parseInt(inp/Math.pow(10,i*3),10);
			if(kharej === 0)
			{
				maxLevel = (i>0)?i-1:0;
				maxLevelFound = true;
			}
		}
		var inp_tmp = inp;
		var out_arr = [];
		for(var i = maxLevel; i >= 0; i--)
		{
			var kharej = parseInt(inp_tmp/Math.pow(10,i*3),10);
			inp_tmp -= kharej*Math.pow(10,i*3);
			out_arr.push(underThous(kharej)+' '+convert_level[i]);
		}
                var out_tmp = [];
                for(var i = 0;i < out_arr.length;i++)
                    if($.trim(out_arr[i])!=='')
                        out_tmp.push(out_arr[i]);
                //console.log(out_arr);
		out = out_tmp.join(' و ');
	}
	return(out);
}