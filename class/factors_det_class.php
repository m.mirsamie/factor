<?php
    class factors_det_class
    {
        public $id = -1;
        public $factors_id = -1;
        public $tarikh = '.........';
        public $shomare = '.........';
        public $peyvast = '.......';
        public $title = '';
        public $header = '';
        public $header_margin = '';
        public $footer = '';
        public $footer_margin = '';
        public $body_json = '';
        public $has_bg = '';
        public $row_7 = '';
        public $row_8 = '';
        public $sum_1 = '';
        public $sum_2 = '';
        public $sum_3 = '';
        public $sum_4 = '';
        public $page_number = '';
        public $pic = '';
        public $benamekhoda = '';
        public $takhfif_darsad = '0';
        public $header_show = '1';
        public $footer_show = '1';
        public $auto_peyvast = '1';
        public $emza = '1';
        public $emza2 = '1';
        public function __construct($id=-1)
        {
            if((int)$id > 0)
            {
                $mysql = new mysql_class;
                $mysql->ex_sql("select * from `factors_det` where `id` = $id",$q);
                if(isset($q[0]))
                {
                    $r = $q[0];
                    $this->id=$r['id'];
                    $this->factors_id=$r['factors_id'];
                    $this->tarikh=$r['tarikh'];
                    $this->shomare=$r['shomare'];
                    $this->peyvast=$r['peyvast'];
                    $this->title=$r['title'];
                    $this->header=$r['header'];
                    $this->header_margin=$r['header_margin'];
                    $this->footer=$r['footer'];
                    $this->footer_margin=$r['footer_margin'];
                    $this->body_json=$r['body_json'];
                    $this->has_bg=$r['has_bg'];
                    $this->row_7=$r['row_7'];
                    $this->row_8=$r['row_8'];
                    $this->sum_1=$r['sum_1'];
                    $this->sum_2=$r['sum_2'];
                    $this->sum_3=$r['sum_3'];
                    $this->sum_4=$r['sum_4'];
                    $this->page_number=$r['page_number'];
                    $this->pic=$r['pic'];
                    $this->benamekhoda=$r['benamekhoda'];
                    $this->takhfif_darsad=$r['takhfif_darsad'];
                    $this->header_show=$r['header_show'];
                    $this->footer_show=$r['footer_show'];
                    $this->auto_peyvast=$r['auto_peyvast'];
                    $this->emza=$r['emza'];
                    $this->emza2=$r['emza2'];
                }
            }
        }
        public function loadByFactor($factor_id,$page_number=-1)
        {
            $out = null;
            $page_number = (int)$page_number;
            if((int)$factor_id > 0)
            {
                $mysql = new mysql_class;
                if($page_number<1)
                {
                    $out = array();
                    $mysql->ex_sql("select * from `factors_det` where `factors_id` = $factor_id",$q);
                    foreach($q as $r)
                    {
                        $thi = new factors_det_class;
                        $thi->id=$r['id'];
                        $thi->factors_id=$r['factors_id'];
                        $thi->tarikh=$r['tarikh'];
                        $thi->shomare=$r['shomare'];
                        $thi->peyvast=$r['peyvast'];
                        $thi->title=$r['title'];
                        $thi->header=$r['header'];
                        $thi->header_margin=$r['header_margin'];
                        $thi->footer=$r['footer'];
                        $thi->footer_margin=$r['footer_margin'];
                        $thi->body_json=$r['body_json'];
                        $thi->has_bg=$r['has_bg'];
                        $thi->row_7=$r['row_7'];
                        $thi->row_8=$r['row_8'];
                        $thi->sum_1=$r['sum_1'];
                        $thi->sum_2=$r['sum_2'];
                        $thi->sum_3=$r['sum_3'];
                        $thi->sum_4=$r['sum_4'];
                        $thi->page_number=$r['page_number'];
                        $thi->pic=$r['pic'];
                        $thi->benamekhoda=$r['benamekhoda'];
                        $thi->takhfif_darsad=$r['takhfif_darsad'];
                        $thi->header_show=$r['header_show'];
                        $thi->footer_show=$r['footer_show'];
                        $thi->auto_peyvast=$r['auto_peyvast'];
                        $thi->emza=$r['emza'];
                        $thi->emza2=$r['emza2'];
                        $out[] = $thi;
                    }
                }
                else
                {
                    $mysql->ex_sql("select * from `factors_det` where `factors_id` = $factor_id and page_number = '$page_number'",$q);
                    if(isset($q[0]))
                    {
                        $r = $q[0];
                        $this->id=$r['id'];
                        $this->factors_id=$r['factors_id'];
                        $this->tarikh=$r['tarikh'];
                        $this->shomare=$r['shomare'];
                        $this->peyvast=$r['peyvast'];
                        $this->title=$r['title'];
                        $this->header=$r['header'];
                        $this->header_margin=$r['header_margin'];
                        $this->footer=$r['footer'];
                        $this->footer_margin=$r['footer_margin'];
                        $this->body_json=$r['body_json'];
                        $this->has_bg=$r['has_bg'];
                        $this->row_7=$r['row_7'];
                        $this->row_8=$r['row_8'];
                        $this->sum_1=$r['sum_1'];
                        $this->sum_2=$r['sum_2'];
                        $this->sum_3=$r['sum_3'];
                        $this->sum_4=$r['sum_4'];
                        $this->page_number=$r['page_number'];
                        $this->pic=$r['pic'];
                        $this->benamekhoda=$r['benamekhoda'];
                        $this->takhfif_darsad=$r['takhfif_darsad'];
                        $this->header_show=$r['header_show'];
                        $this->footer_show=$r['footer_show'];
                        $this->auto_peyvast=$r['auto_peyvast'];
                        $this->emza=$r['emza'];
                        $this->emza2=$r['emza2'];
                    }
                    $out = $this;
                }
            }
            return($out);
        }
        public function addFactor_det($inp,$typ)
        {
            $my  = new mysql_class;
            $factors_id = $inp->factors_id;
            $tarikh = mysql_real_escape_string($inp->tarikh);
            $shomare = mysql_real_escape_string($inp->shomare);
            $peyvast=mysql_real_escape_string($inp->peyvast);
            $title = mysql_real_escape_string($inp->title);
            $header = mysql_real_escape_string($inp->header);
            $header_margin = $inp->header_margin;
            $footer = mysql_real_escape_string($inp->footer);
            $footer_margin= $inp->footer_margin;
            $body_json = mysql_real_escape_string($inp->body_json);
            $has_bg = $inp->has_bg;
            $row_7 = $inp->row_7;
            $row_8 = $inp->row_8;
            $sum_1 = $inp->sum_1;
            $sum_2 = $inp->sum_2;
            $sum_3 = $inp->sum_3;
            $sum_4 = $inp->sum_4;
            $pic = $inp->pic;
            $benamekhoda=$inp->benamekhoda;
            $takhfif_darsad=$inp->takhfif_darsad;
            $header_show=$inp->header_show;
            $footer_show=$inp->footer_show;
            $page_number = $inp->page_number;
            $auto_peyvast = $inp->auto_peyvast;
            $emza = $inp->emza;
            $emza2 = $inp->emza2;
            if($typ>0)
            {
                //update
                $my->ex_sqlx("update factors_det set `factors_id`='$factors_id', `tarikh`='$tarikh', `shomare`='$shomare', `peyvast`='$peyvast', `title`='$title', `header`='$header', `header_margin`='$header_margin', `footer`='$footer', `footer_margin`='$footer_margin', `body_json`='$body_json', `has_bg`='$has_bg', `row_7`='$row_7', `row_8`='$row_8', `sum_1`='$sum_1', `sum_2`='$sum_2', `sum_3`='$sum_3', `sum_4`='$sum_4', `page_number`='$page_number' , `pic` = '$pic' , `benamekhoda` = '$benamekhoda' , `takhfif_darsad` = '$takhfif_darsad' , `header_show` = '$header_show' , `footer_show` = '$footer_show' ,`auto_peyvast` = '$auto_peyvast',`emza` = '$emza',`emza2` = '$emza2' where id=$typ") ;
            }
            else
            {
                //insert
                $my->ex_sqlx("insert into factors_det (`factors_id`, `tarikh`, `shomare`, `peyvast`, `title`, `header`, `header_margin`, `footer`, `footer_margin`, `body_json`, `has_bg`, `row_7`, `row_8`, `sum_1`, `sum_2`, `sum_3`, `sum_4`, `page_number`,`pic`,`benamekhoda` , `takhfif_darsad` , `header_show` , `footer_show`,`auto_peyvast`,`emza`,`emza2`) value ('$factors_id', '$tarikh', '$shomare', '$peyvast', '$title', '$header', '$header_margin', '$footer', '$footer_margin', '$body_json', '$has_bg', '$row_7', '$row_8', '$sum_1', '$sum_2', '$sum_3', '$sum_4', '$page_number','$pic','$benamekhoda' , '$takhfif_darsad' ,'$header_show' ,'$footer_show','$auto_peyvast','$emza','$emza2')");
            }    
        }
    }