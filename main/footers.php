<?php
        include_once("../kernel.php");
        include_once('../simplejson.php');
        function loadToz($inp)
        {
            $tt = new footers_class($inp);
            $out = '<div style="display:none;" id="div_'.$inp.'" >'.$tt->toz.'</div><span>'.  strip_tags($tt->toz).'</span> <a class="btn btn-default" href="#" onclick=\'startEdit("'.$tt->id.'")\'; ><span class="glyphicon glyphicon-pencil" ></span></a>';
            return($out);
        }
        if(isset($_REQUEST['toz']))
        {
            $id = $_REQUEST['edit_id'];
            $my = new mysql_class;
            $my->ex_sqlx("update footers set toz='".$_REQUEST['toz']."' where id=$id");
        }
        $gname = "gname_footers";
	$input =array($gname=>array('table'=>'footers','div'=>'main_div_footers'));
        $xgrid = new xgrid($input);
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'توضیحات';
        $xgrid->column[$gname][1] = $xgrid->column[$gname][0];
	$xgrid->column[$gname][1]['name'] = 'شرح';
        $xgrid->column[$gname][1]['cfunction'] = array('loadToz');
        $xgrid->column[$gname][1]['access']='1';
	$xgrid->column[$gname][2]['name'] = 'نام';
        $xgrid->canAdd[$gname] = TRUE;
        $xgrid->canDelete[$gname] = TRUE;
        $xgrid->canEdit[$gname] = TRUE;
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);
?>
<!DOCTYPE html>
<html>
    <head>
        <script src="../js/jquery.min.js" ></script>
        <script src="../js/grid.js" ></script>
        <script src="../js/bootstrap.min.js" ></script>
        <script type="text/javascript" >
            $(document).ready(function(){
                    var args=<?php echo $xgrid->arg; ?>;
                    intialGrid(args);
            });
        </script>
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/bootstrap-rtl.min.css">
        <link rel="stylesheet" href="../css/myapp.css">
        <link rel="stylesheet" href="../css/xgrid.css">
        <link rel="stylesheet" type="text/css" media="all" href="../js/cal/skins/aqua/theme.css" title="Aqua" />
        <script type="text/javascript" src="../js/cal/jalali.js"></script>
        <script type="text/javascript" src="../js/cal/calendar.js"></script>
        <script type="text/javascript" src="../js/cal/calendar-setup.js"></script>
        <script type="text/javascript" src="../js/cal/lang/calendar-fa.js"></script>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body dir="rtl">
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">ویرایش متن</h4>
                </div>
                <div class="modal-body">
                    <form id="frm1" method="POST" >
                      <div class="row gc-padding2 gc-border">
                          <textarea id="tmp_area" name="toz" contenteditable="true" ></textarea>
                          <input type="hidden" id="edit_id" name="edit_id" >
                      </div>
                      <button class="btn btn-default" >ثبت</button>
                      <a href="#" class="btn btn-default" data-dismiss="modal">بستن</a>
                    </form>
                </div>
              </div>
            </div>
        </div>
        <?php echo $conf->header; ?>
        <div id="main_div_footers"></div>
        <?php echo $conf->footer; ?>
    </body>
    <script src="../js/ckeditor.js" ></script>
    <script src="../js/edit.js" ></script>
</html>
