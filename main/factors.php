<?php
        include_once("../kernel.php");
        function add_factor($gname,$table,$fields,$cl){
            $pish = (isset($_REQUEST['pish']) && (int)$_REQUEST['pish']==1)?1:0;
            $my = new mysql_class;
            $my->ex_sqlx("insert into factors (name,tarikh,shomare,typ) values ('".$fields['name']."','".tarikh_back($fields['tarikh'])."','".$fields['shomare']."','$pish')");
            return(TRUE);
        }
        function loadFactor($id){
            return('<a target="_blank" href="factor.php?factor_id='.$id.'&page_number=1&">ادامه</a>');
        }
        function tarikh($inp)
        {
            return(($inp!='' && $inp!='0000-00-00 00:00:00')?jdate("Y/m/d",strtotime($inp)):'----');
        }
        function tarikh_back($inp)
        {
            return(hamed_pdateBack2($inp));
        }
        $pish = 0;
        if(isset($_REQUEST['pish']))
        {
            $pish = (int)$_REQUEST['pish'];
        }        
        $gname = "gname_factors";
	$input =array($gname=>array('table'=>'factors','div'=>'main_div_factors'));
        $xgrid = new xgrid($input);
        $xgrid->whereClause[$gname] = " typ = '$pish' ";
        $xgrid->eRequest[$gname] = array("pish"=>$pish);
	$xgrid->column[$gname][0]['name'] = '';
	$xgrid->column[$gname][1]['name'] = 'موضوع';
	$xgrid->column[$gname][2]['name'] = 'تاریخ';
        $xgrid->column[$gname][2]['cfunction'] = array('tarikh','tarikh_back');
	$xgrid->column[$gname][3]['name'] = 'شماره';
	$xgrid->column[$gname][4]['name'] = 'حالت';
        //$xgrid->column[$gname][4]['access'] = 'a';
        $xgrid->column[$gname][4]['clist'] = array('1'=>'پیش فاکتور','2'=>'فاکتور');
        $xgrid->column[$gname][] = $xgrid->column[$gname][0];
        $xgrid->column[$gname][5]['access'] = 'a';
        $xgrid->column[$gname][5]['name'] = 'جزئیات';
        $xgrid->column[$gname][5]['cfunction'] = array('loadFactor');
        $xgrid->canAdd[$gname] = TRUE;
        $xgrid->canDelete[$gname] = TRUE;
        $xgrid->canEdit[$gname] = TRUE;
        $xgrid->addFunction[$gname] = 'add_factor';
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);
?>
<!DOCTYPE html>
<html>
    <head>
        <script src="../js/jquery.min.js" ></script>
        <script src="../js/grid.js" ></script>
        <script type="text/javascript" >
            $(document).ready(function(){
                    var args=<?php echo $xgrid->arg; ?>;
                    intialGrid(args);
            });
        </script>
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/bootstrap-rtl.min.css">
        <link rel="stylesheet" href="../css/myapp.css">
        <link rel="stylesheet" href="../css/xgrid.css">
        <link rel="stylesheet" type="text/css" media="all" href="../js/cal/skins/aqua/theme.css" title="Aqua" />
        <script type="text/javascript" src="../js/cal/jalali.js"></script>
        <script type="text/javascript" src="../js/cal/calendar.js"></script>
        <script type="text/javascript" src="../js/cal/calendar-setup.js"></script>
        <script type="text/javascript" src="../js/cal/lang/calendar-fa.js"></script>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body dir="rtl">
        <?php echo $conf->header; ?>
        <div id="main_div_factors"></div>
        <?php echo $conf->footer; ?>
    </body>
</html>
