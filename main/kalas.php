<?php
        include_once("../kernel.php");
        include_once('../simplejson.php');
        function loadGallery($id)
	{
		$id = (int)$id;
		$out = '----';
		$my = new mysql_class;
		$my->ex_sql("select `pic` from `kalas` where `id` = '$id'",$q);
		if(isset($q[0]))
		{
			if ($q[0]["pic"]!='')
			{
				$pic = $q[0]["pic"];
				$out = "<img src=\"$pic\" class=\"imgGallTmb pointer\" onclick=\"startUpload1('$id')\"/>";
				$out .= "<div id=\"fileupload1_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr1_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
			else
			{
				$out = "<button  style=\"margin:10px;\" onclick=\"startUpload('$id');\">بروزرسانی تصویر</button>";
				$out .= "<div id=\"fileupload_$id\" style=\"display:none;\">";
					$out .= "<iframe scrolling=\"no\" id=\"ifr_$id\" style=\"border:solid 1px #e0e0e0;width:93px;height:73px;\">";
					$out .= "</iframe>";
				$out .= "</div>";
			}
		}
		return($out);
	}
        function add($gname,$table,$fields,$column)
        {
            $conf = new conf;
            $my = new mysql_class;
            $name = str_replace('"','', $fields['name']);
            $onvan = str_replace('"','', $fields['onvan']);
            $ghimat = (int)$fields['ghimat'];
            $my->ex_sqlx("insert into $table (name,onvan,ghimat,ordering) values ('$name','$onvan','$ghimat',".$fields['ordering'].")");
            return(TRUE);
        }
        function loadToz($inp)
        {
            $tt = new kalas_class($inp);
            $out = '<div style="display:none;" id="div_'.$inp.'" >'.$tt->toz.'</div><span>'.  strip_tags($tt->toz).'</span> <a class="btn btn-default" href="#" onclick=\'startEdit("'.$tt->id.'")\'; ><span class="glyphicon glyphicon-pencil" ></span></a>';
            return($out);
        }
        if(isset($_REQUEST['toz']))
        {
            $id = $_REQUEST['edit_id'];
            $my = new mysql_class;
            $my->ex_sqlx("update kalas set toz='".$_REQUEST['toz']."' where id=$id");
        }    
        $gname = "gname_footers";
	$input =array($gname=>array('table'=>'kalas','div'=>'main_div_footers'));
        $xgrid = new xgrid($input);
        $xgrid->whereClause[$gname] = "1=1 order by ordering,id";
	$xgrid->column[$gname][0]['name'] = '';
        $xgrid->column[$gname][1] = $xgrid->column[$gname][0];
	$xgrid->column[$gname][1]['name'] = 'شرح';
        $xgrid->column[$gname][1]['cfunction'] = array('loadToz');
	$xgrid->column[$gname][2]['name'] = 'مدل';
        $xgrid->column[$gname][1]['access']='1';
        $xgrid->column[$gname][3]['name'] = 'عنوان';
        $xgrid->column[$gname][4]['name'] = 'قیمت';
        $xgrid->column[$gname][5]['name'] = '';
        $xgrid->column[$gname][6]['name'] = 'ترتیب';
        $xgrid->column[$gname][] = $xgrid->column[$gname][0];
        $xgrid->column[$gname][7]['name'] = 'تصویر';
	$xgrid->column[$gname][7]['cfunction'] = array('loadGallery');
	$xgrid->column[$gname][7]['access'] = 'a';
        $xgrid->canAdd[$gname] = TRUE;
        $xgrid->addFunction[$gname] = 'add';
        $xgrid->canDelete[$gname] = TRUE;
        $xgrid->canEdit[$gname] = TRUE;
        $out =$xgrid->getOut($_REQUEST);
        if($xgrid->done)
                die($out);
?>
<!DOCTYPE html>
<html>
    <head>
        <script src="../js/jquery.min.js" ></script>
        <script src="../js/grid.js" ></script>
        <script src="../js/bootstrap.min.js" ></script>
        <script type="text/javascript" >
            $(document).ready(function(){
                    var args=<?php echo $xgrid->arg; ?>;
                    intialGrid(args);
            });
            function startUpload(id)
            {
                    $("#ifr_"+id).prop("src","upload_pic.php?id="+id+"&");
                    $("#fileupload_"+id).toggle();
            }
            function startUpload1(id)
            {
                    $("#ifr1_"+id).prop("src","upload_pic.php?id="+id+"&");
                    $("#fileupload1_"+id).toggle();
            }
            function RPage()
            {
                window.location = window.location;
            }
        </script>
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/bootstrap-rtl.min.css">
        <link rel="stylesheet" href="../css/myapp.css">
        <link rel="stylesheet" href="../css/xgrid.css">
        <link rel="stylesheet" type="text/css" media="all" href="../js/cal/skins/aqua/theme.css" title="Aqua" />
        <script type="text/javascript" src="../js/cal/jalali.js"></script>
        <script type="text/javascript" src="../js/cal/calendar.js"></script>
        <script type="text/javascript" src="../js/cal/calendar-setup.js"></script>
        <script type="text/javascript" src="../js/cal/lang/calendar-fa.js"></script>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body dir="rtl">
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">ویرایش متن</h4>
                </div>
                <div class="modal-body">
                    <form id="frm1" method="POST" >
                      <div class="row gc-padding2 gc-border">
                          <textarea id="tmp_area" name="toz" contenteditable="true" ></textarea>
                          <input type="hidden" id="edit_id" name="edit_id" >
                      </div>
                      <button class="btn btn-default" >ثبت</button>
                      <a href="#" class="btn btn-default" data-dismiss="modal">بستن</a>
                    </form>
                </div>
              </div>
            </div>
        </div>
        <div id="main_div_footers"></div>
    </body>
    <script src="../js/ckeditor.js" ></script>
    <script src="../js/edit.js" ></script>
</html>
