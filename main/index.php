<?php
    include_once('../kernel.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <script src="../js/jquery.min.js" ></script>
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/bootstrap-rtl.min.css">
        <link rel="stylesheet" href="../css/myapp.css">
        <meta charset="UTF-8">
        <title>سامانه صدور فاکتور بهار</title>
    </head>
    <body dir="rtl">
            <?php echo $conf->header; ?>
            <div class="row" >
                <div class="col-lg-6  text-center" >
                    <h3 class="gc-green2 gc-padding20" ><a target="_blank" href="factors.php?pish=1&"  >صدورپیش فاکتور</a></h3>
                </div>
                <div class="col-lg-6 text-center" >
                    <h3 class="gc-red gc-padding20">
                        <a target="_blank" href="factors.php?pish=2&" >صدور فاکتور</a>
                    </h3>
                </div>
            </div>
            <div class="row" >
                <div class="col-lg-6 text-center" >
                    <h3 class="gc-perpul gc-padding20">
                        <a target="_blank" href="footers.php"  >ثبت پاورقی</a>
                    </h3>
                </div>
                <div class="col-lg-6 text-center" >
                    <h3 class="gc-color1 gc-padding20" >
                        <a target="_blank" href="kalas.php" >ثبت کالای پیش فرض</a>                        
                    </h3>
                </div>
            </div>
            <?php echo $conf->footer; ?>
    </body>
</html>