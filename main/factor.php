<?php
    include '../kernel.php';
    //include '../simplejson.php';
    $factor_id = isset($_REQUEST['factor_id'])?(int)$_REQUEST['factor_id']:-1;
    $page_number = isset($_REQUEST['page_number'])?(int)$_REQUEST['page_number']:1;
    $factor_det = new factors_det_class();
    $factor_det->factors_id = $factor_id;
    $factor_det->page_number = $page_number;
    if(isset($_POST['gc-data']))
    {
        $gc_data = json_decode($_POST['gc-data']);
    //var_dump($gc_data);
    //die();
        factors_det_class::addFactor_det($gc_data,$gc_data->id);
    }
    $factor_det->loadByFactor($factor_id, $page_number);
    $allPageNumbers = factors_class::loadPageNumbers($factor_id);
    if($factor_det->auto_peyvast>0)
    {    
        $factor_det->peyvast = $page_number.' از '.(((int)$allPageNumbers>=(int)$page_number)?$allPageNumbers:$page_number);
    }    
?>
<!DOCTYPE html>
<html>
    <head>
        <script>
            var json_url="<?php echo $conf->site_url?>factor.php";
            var selected_obj;
            var selected_text='';
            var factor=<?php echo json_encode($factor_det) ?>;
            var editor;
            function showBackground(inp){
                $(".gc-background").each(function(id,field){
                    if(parseInt(inp,10)===1)
                    {
                        $("#"+$(field).prop('id')+" img").show();
                        $(".gc-top-left-4").removeClass('gc-top-left-4');
                        $(".gc-top-left").addClass('gc-top-left-3');
                    }
                    else
                    {
                        $("#"+$(field).prop('id')+" img").hide();
                        $(".gc-top-left-3").removeClass('gc-top-left-3');
                        $(".gc-top-left").addClass('gc-top-left-4');
                    }
                });
                //changeFooterHeader();
            }
            function changefont(obj)
            {
                var gc_font= $(obj).val();
                $('html, body').css("font-family",gc_font);
            }
            function change_font_size()
            {
                var gc_font= $("#gc-font-size").val()+"px";
                $('html, body').css("font-size",gc_font);
            }
            function change_header_margin(inp)
            {
                var tmp;
                if(typeof inp==='undefined')
                    tmp = factor.header_margin;
                else
                {    
                    tmp = $("#header_margin").val();
                    factor.header_margin = tmp;
                }    
                $("#header").css("margin-top",tmp+"px");
            }
            function changeFooterHeader()
            {
                if(parseInt(factor.header_show,10)===1)
                {    
                    $("#header").show(); 
                }
                else
                    $("#header").hide();
                if(parseInt(factor.footer_show,10)===1)
                {    
                    $("#footer").show(); 
                }
                else
                    $("#footer").hide();
            }
            function change_footer_margin(inp)
            {
                var tmp;
                if(typeof inp==='undefined')
                    tmp = factor.footer_margin;
                else
                {    
                    tmp = $("#footer_margin").val();
                    factor.footer_margin = tmp;
                }    
                $("#footer").css("margin-top",tmp+"px");
            }
            function changePage()
            {
                if(confirm("آیا اطلاعات جاری را ذخیره  کرده اید در غیر این صورت اطلاعات از دست خواهد رفت"))
                {    
                    var pgn = $("#page_number").val();
                    window.location = '<?php echo $conf->site_url; ?>'+'main/factor.php'+'?factor_id='+factor.factors_id+'&page_number='+pgn+'&';
                }
                else
                {
                    $("#page_number").val(factor.page_number);
                }
            }
            function toggle_emza()
            {
                if($("#emza").prop("checked"))
                    $("#emza_div").show();
                else
                    $("#emza_div").hide();
            }
            function toggle_emza2()
            {
                if($("#emza2").prop("checked"))
                    $("#emza2_div").show();
                else
                    $("#emza2_div").hide();
            }
            function toggle_header(obj)
            {
                //console.log($("#header"));
                /*
                console.log($(obj).prop('checked'));
                if($(obj).prop('checked')===true)
                {
                */
                /*
                }        
                else
                {    
                    $("#header").hide();
                } 
                */
            }
            function toggle_footer(obj)
            {
                    if($(obj).prop('checked'))
                            $("#footer").show();
                    else
                            $("#footer").hide();
            }
        </script>
        <script src="../js/jquery.min.js" ></script>
        <script src="../js/drawer.js" ></script>
        <script src="../js/ckeditor.js" ></script>
        <script src="../js/bootstrap.min.js" ></script>
        <script src="../js/Horof.js" ></script>
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/bootstrap-rtl.min.css">
        <link rel="stylesheet" href="../css/myapp.css">
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body dir="rtl">
        <div class="gc-controller gc-border gc-margin-top" >
            <form id="form_data" method="POST" action="factor.php"  >
                <textarea id="gc-data" name="gc-data" style="display: none;" ></textarea>
                <input type="hidden" name="page_number" value="<?php echo $page_number; ?>" >
                <input type="hidden" name="factor_id" value="<?php echo $factor_id; ?>" >
            </form>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ویرایش متن</h4>
      </div>
      <div class="modal-body">
            <div class="row gc-padding2 gc-border">
                <textarea id="tmp_area" contenteditable="true" ></textarea>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
      </div>
    </div>
  </div>
</div>
            <div class="alert alert-danger">
               ویرایش  تنظیمات فاکتور
            </div>
            <div class="row gc-padding2 gc-border">
                <textarea id="tmp_area1"  ></textarea>
            </div>
            <div class="row gc-padding2">
                <button class="btn btn-success" onclick="removeRow();" >حذف سطر</button>
                <button class="btn btn-success" onclick="addRow();" >افزودن سطر</button>
            </div>
            <div class="row gc-padding2">
                نوع فونت
                <select onchange="changefont(this);" >
                    <option value="DroidNaskh" >دروید نسخ</option>
                    <option value="BYekan" >یکان</option>
                    <option value="BNazanin" >نازنین</option>
                    <option value="Mitra" >میترا</option>
                    <option value="BTitr" >تیتر</option>
                </select>
            </div>
            <div class="row gc-padding2">
                اندازه فونت
                <input type="number" id="gc-font-size" value="14" style="width: 80px;" onblur="change_font_size();" >
            </div>
            <div class="row gc-padding2">
                توضیحات کالا 
                <select id="select_kala" onchange="matnChange(this);" >
                    <option value="" >انتخاب</option>
                    <?php echo kalas_class::loadAll(); ?>
                </select>
            </div>
            <div class="row gc-padding2">
                 متن فوتر
                 <select onchange="footerChange(this);">
                     <option value="" >انتخاب</option>
                    <?php echo footers_class::loadAll(); ?>
                </select>
            </div>
            <div class="row gc-padding2">
                 صفحه
                <input type="number" id="page_number" class="hasValue" value="<?php echo $page_number; ?>" style="width: 80px;"  >
                <button class="btn btn-success" onclick="changePage();" >تغییر صفحه</button>
            </div>
            <div class="row gc-padding2">
                 فاصله عنوان از بالا
                <input type="number" id="header_margin" class="hasValue" value="0" style="width: 60px;" onblur="change_header_margin(this);" >px
            </div>
            <div class="row gc-padding2">
                 فاصله فوتر ازپایین
                <input type="number" id="footer_margin" class="hasValue" value="0" style="width: 60px;" onblur="change_footer_margin(this);" >px
            </div>
            <div class="row gc-padding2">
                <input type="checkbox" id="has_bg" class="config" > 
                 نمایش سربرگ
            </div>
            <div class="row gc-padding2">
                <input type="checkbox" id="header_show" class="config" > 
                 نمایش هدر
            </div>
            <div class="row gc-padding2">
                <input type="checkbox" id="footer_show" class="config" > 
                 نمایش فوتر
            </div>
            <div class="row gc-padding2">
                <input type="checkbox" id="row_7" class="config" >
                نمایش ستون تخفیف
            </div>
            <div class="row gc-padding2"  >
                <input type="checkbox" id="row_8" class="config" >
                 نمایش ستون تصویر
            </div>
            <div class="row gc-padding2" >
                <input type="checkbox" id="takhfif_darsad" class="config" >
                 تخفیف درصدی
            </div>
            <div class="row gc-padding2">
                <input type="checkbox" id="tick_sum_1" class="config tick" >
                نمایش  جمع کل
            </div>
            <div class="row gc-padding2">
                <input type="checkbox" id="tick_sum_2" class="config tick" >
                نمایش  تخفیف
            </div>
            <div class="row gc-padding2">
                <input type="checkbox" id="tick_sum_3" class="config tick" >
                نمایش  مالیات
            </div>
            <div class="row gc-padding2">
                <input type="checkbox" id="tick_sum_4" class="config tick" >
                نمایش  قابل پرداخت
            </div>
            <div class="row gc-padding2">
                <input type="checkbox" id="auto_peyvast" class="config" >
                تولید خودکار متن پیوست
            </div>
            <div class="row gc-padding2">
                <input type="checkbox" id="emza" class="config" onclick="toggle_emza()"  >
                نمایش امضا
            </div>
            <div class="row gc-padding2">
                <input type="checkbox" id="emza2" class="config" onclick="toggle_emza2()"  >
                نمایش امضا ۲
            </div>
            <div class="row gc-padding2">
                <button class="btn btn-danger" onclick="sendFactor();" >ذخیره</button>
            </div>
        </div>
        <div class="page_a4 gc-background" id="back_div" style="position: fixed;z-index: -2;top: 0;">
            <img src="../img/water-bg.jpg" style="height:29.5cm" > 
        </div>
        <div class="page_a4">
            <div class="row" >
                <div class="gc-top-right gc-background" id="top-right"  >
                    <img src="../img/top.jpg" style="height:2.4cm;" >
                </div>
                <div class="gc-top-center"  >
                    <div id="benamekhoda" class="editable title" style="width:100px;font-family:'IranNastaliq';font-size:26px; " >به نام خدا</div>
                   <div id="title" style="margin-top: 0.5cm;" class="editable title" >title</div>
                </div>
                <div class="gc-top-left col-lg-1" >
                    <div>
                         <span id="tarikh" class="editable" ></span>
                    </div>
                    <div>
                         <span id="shomare" class="editable" ></span>
                    </div>
                    <div>
                         <span id="peyvast" class="editable" ></span>
                    </div>
                </div>
            </div>
            
            <div class="gc-border gc-padding editable  gc-margin-right-left" id="header" >header</div>
            <div class="gc-margin-right-left" >
                <table style="width: 100%" >
                    <tr class="maintable_odd ">
                        <th class="gc-1">ردیف</th>
                        <th class="gc-2" >شرح کالا</th>
                        <th class="gc-3">مدل</th>
                        <th class="gc-4">تعداد</th>
                        <th class="gc-5">قیمت واحد
                             (ریال)</th>
                        <th class="gc-7 text-center">تخفیف</th>
                        <th class="gc-6 text-center">
                            قیمت کل  
                             (ریال)</th>
                        <th class="gc-8">تصویر</th>
                    </tr>
                    <tr class="maintable_odd maintable_row row_0">
                        <td class="gc-1">1</td>
                        <td class="gc-2"></td>
                        <td class="gc-3"></td>
                        <td class="gc-4"></td>
                        <td class="gc-5"></td>
                        <td class="gc-7"></td>
                        <td class="gc-6"></td>
                        <td class="gc-8"></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-right td_sum" id="jam_kol" >
                           جمع کل
                        </td>
                        <td id="sum_1" class="editable" style="border-left: none;" ></td>
                        <td style="border-right: none;" ></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-right td_sum" id="takhfif_kol" >
                            تخفیف
                        </td>
                        <td id="sum_2" class="editable" style="border-left: none;" ></td>
                        <td style="border-right: none;" ></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-right td_sum" id="tax" >
                            مالیات برارزش افزوده
                        </td>
                        <td id="sum_3" style="border-left: none;" ></td>
                        <td style="border-right: none;" ></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-right td_sum" id="ghabel_pardakht"  >
                            قابل پرداخت
                        </td>
                        <td id="sum_4" style="border-left: none;" class="editable" ></td>
                        <td style="border-right: none;" ></td>
                    </tr>
                </table>
            </div>
            <div class="gc-border gc-padding gc-footer editable gc-margin-right-left" id="footer" >footer</div>
            <div class="gc-margin-right-left" id="emza_div" <?php echo (($factor_det->emza>0)?'':'style="display:none"'); ?> ><img style="width: 4cm;float: left;" src="../img/emza.png" ></div>
            <div class="gc-margin-right-left" id="emza2_div" <?php echo (($factor_det->emza2>0)?'':'style="display:none"'); ?> ><img style="width: 4cm;float: left;" src="../img/emza2.png" ></div>
            <div class="gc-down-page gc-background" id="down_page" >
                <img src="../img/footer.jpg" style="height: 1.888cm;" >
            </div>
        </div>
    </body>
</html>
